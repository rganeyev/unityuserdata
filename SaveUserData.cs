﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
using System.Text;

public class SaveUserData
{
	private const string URL = "http://laikabossgames.com/gamelinks/adduser/";
	private static bool sent = false;

	public static void SendToServer (UnityUserData data)
	{
		if (sent)
			return;
		var dictionary = data.Save ();
		var stringDictionary = new Dictionary<string, string> ();
		foreach (var item in dictionary)
			stringDictionary.Add (item.Key, item.Value.ToString ());

		Loom.Download (URL, stringDictionary, result => {
			if (!result.HasError)
				sent = true;
		});
	}
}

public class UnityUserData: SerializableClass
{
	public UnityUserData (string id, string token)
	{
		this.user_id = id;
		this.push_token = token;
	}

	public string user_id;
	public string push_token;

	public string BundleId { get { return BundleUtilities.BundleIdentifier; } }

	public string Platform { get { return UnityExtensions.platform.ToString (); } }

	public override Dictionary<string, object> Save ()
	{
		var result = new Dictionary<string, object> ();
		result.Add ("user_id", user_id);
		result.Add ("push_token", push_token);
		result.Add ("bundle_id", BundleId);
		result.Add ("platform", Platform);
		result.Add ("email", EMail);

		return result;
	}

	public static string EMail {
		get { 
			#if UNITY_ANDROID && !UNITY_EDITOR
			try {
				using (var account_mngr = new AndroidJavaClass("android.accounts.AccountManager")) {
					using (var jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer")) {
						var jo = jc.GetStatic<AndroidJavaObject> ("currentActivity");
						var context_obj = jo.Call<AndroidJavaObject> ("getApplication");
						var mngr = account_mngr.CallStatic<AndroidJavaObject> ("get", context_obj);

						var arr = mngr.Call<AndroidJavaObject> ("getAccountsByType", "com.google");
						var ob = AndroidJNIHelper.ConvertFromJNIArray<AndroidJavaObject[]> (arr.GetRawObject ());

						var sb = new StringBuilder ();
						int cnt = ob.Length;
						for (int i = 0; i < cnt; ++i) {
							sb.Append (ob [i].Get<string> ("name"));
							if (i != cnt - 1)
								sb.Append (",");
						}

						return sb.ToString ();
					}
				}
			} catch (Exception ex) {
				Debug.LogException (ex);
				return string.Empty;
			}
			#else
			return "";
			#endif
		}
	}
}
